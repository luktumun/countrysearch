import React from "react";
import { useState, useEffect } from "react";
import "./CountaryFlags.css";

const CountaryFlags = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const convertMapToArray = (map) => {
    return Array.from(map, ([name, value, value1]) => {
      return { name, value, value1 };
    }); // convert using Array.from
  };

  const map1 = new Map();
  for (let i = 0; i < data.length; i++) {
    map1.set(data[i].name.common, data[i].flags.png, data[i].flags.alt);
  }

  const convertedArray = convertMapToArray(map1); // function call
  for (let i = 0; i < 10; i++) {
    console.log(convertedArray[i]);
  }

  const [isOpen, setIsOpen] = useState(true);

  const [filteredUsers, setFilteredUsers] = useState(convertedArray);
  const handleFilter = (event) => {
    const value = event.target.value;
    const filtered = convertedArray.filter((itm) =>
      itm.name.toLowerCase().includes(value.toLowerCase())
    );
    setFilteredUsers(filtered);
    if (filtered) {
      setIsOpen(false);
    }
  };

  return (
    <>
      <input
        className="search_itm"
        placeholder="Search for countries"
        type="text"
        onChange={handleFilter}
      />
      <div>
        {filteredUsers.map((item) => (
          <li className="countryCard" key={item.name} value={item.name}>
            <div className="card-image">
              <img src={item.value} alt={item.value1} />
            </div>

            <span>{item.name}</span>
          </li>
        ))}
      </div>

      <div style={{ display: isOpen ? "block" : "none" }}>
        {convertedArray &&
          convertedArray.map((item) => (
            <li className="countryCard" key={item.name} value={item.name}>
              <div className="card-image">
                <img src={item.value} alt={item.value1} />
              </div>

              <span>{item.name}</span>
            </li>
          ))}
      </div>
    </>
  );
};

export default CountaryFlags;
